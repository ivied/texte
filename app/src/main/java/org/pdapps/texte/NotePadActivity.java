package org.pdapps.texte;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.devpaul.filepickerlibrary.FilePicker;
import com.devpaul.filepickerlibrary.FilePickerActivity;
import com.devpaul.filepickerlibrary.FilePickerBuilder;
import com.devpaul.filepickerlibrary.enums.FileScopeType;

import org.pdapps.texte.SearchTask.OnSearchFinishedListener;
import org.pdapps.texte.SearchTask.Record;
import org.pdapps.texte.customMenu.CheckableMenuItemsEnum;
import org.pdapps.texte.customMenu.MenuAdapter;
import org.pdapps.texte.customMenu.MenuItem;
import org.pdapps.texte.keyboard.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import jp.sblo.pandora.jota.KeywordHighlght;
import jp.sblo.pandora.jota.text.EditText;
import jp.sblo.pandora.jota.text.Layout;

public class NotePadActivity extends AppCompatActivity {

    //// TODO: make this not constant
    public static final int VIEW_MENU_LENGTH = 800;
    public static final int FILE_MENU_LENGTH = 400;

    private static final String DIMENSION_BYTE = "B";
    private static final String DIMENSION_KILOBYTE = "Kb";
    private static final String DIMENSION_MEGABYTE = "Mb";
    private static final String CHAR_SET = "CHAR_SET";
    private static final String FILE_NAME = "FILE_NAME";

    private final String TAG = "NotePadActivity";
    private EditText mTextEditor;
    private int startLetterIndex;
    private float startXForSelection;
    private int startLineForSelection;
    private List<MenuItem> menuItemList;
    private SharedPreferences sharedPreferences;
    boolean isKeyboardVisible = false;
    boolean doubleTouched;
    boolean automaticIndentation;
    GestureDetector gestureDetector;

    private boolean mSearchForward;
    private boolean isTouchSessionUnbroken; //TODO: Probally deprecated.

    private UIUpdater mUIUpdater;
    private ImageButton mBtnClose;
    private ImageButton mBtnBackward;
    private ImageButton mBtnForward;
    private LinearLayout mLlSearch;
    private android.widget.EditText mEdtSearchWord;
    private Button mBtnFile;
    private Button mBtnView;
    private TextView contextMenu;

    private TextView tvLinesValue;
    private TextView tvColumnsValue;
    private TextView tvSelectionValue;
    private TextView tvFileSizeValue;

    private TextView tvLinesText;
    private TextView tvColumnText;
    private TextView tvSelection;
    private TextView tvFileSize;

    private Integer currentLine;
    private Integer totalLineCount;

    private Integer currentColumn;
    private Integer totalColumnCount;

    private Integer startSelectionIndex;
    private Integer endSelectionIndex;
    private Integer selectionSize;
    private Integer selectedLine;
    private Integer selectedOffsetForLine;

    private Double fileSize;
    private String dimension;

    static final String FIRST_ITEM_VALUE = "First item";
    static final int FIRST_ITEM_NUMBER = 1;

    static final String SECOND_ITEM_VALUE = "Second item";
    static final int SECOND_ITEM_NUMBER = 2;

    static final String THIRD_ITEM_VALUE = "Third item";
    static final int THIRD_ITEM_NUMBER = 3;

    public static final int FUZZY_DISTANCE = 10;

    GestureDetector longPressGesture;


    private ArrayList<Record> mSearchResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_pad);
        initiateStatusBar();
        final LinearLayout mainView = (LinearLayout) findViewById(R.id.main_view);
        mLlSearch = (LinearLayout) findViewById(R.id.search);
        mTextEditor = (EditText) findViewById(R.id.text_editor);
        mBtnForward = (ImageButton) findViewById(R.id.btnForward);
        mBtnBackward = (ImageButton) findViewById(R.id.btnBackward);
        mBtnClose = (ImageButton) findViewById(R.id.btnClose);
        mEdtSearchWord = (android.widget.EditText) findViewById(R.id.edtSearchWord);
        mBtnFile = (Button) findViewById(R.id.btnFile);
        mBtnView = (Button) findViewById(R.id.btnView);
        contextMenu = (TextView) findViewById(R.id.context_menu);
        menuItemList = getMenuItemList();
        sharedPreferences = getPreferences(MODE_PRIVATE);
        automaticIndentation = sharedPreferences.getBoolean(CheckableMenuItemsEnum.AUTOMATIC_INDENTATION.toString(), false);
        View touchFilterView = findViewById(R.id.touchFilter);

        touchFilterView.setOnTouchListener(touchFilter(mainView));

        touchFilterView.setOnHoverListener(new View.OnHoverListener() {
            @Override
            public boolean onHover(View v, MotionEvent event) {
                return false;
            }
        });

        mEdtSearchWord.addTextChangedListener(searchTextWatcher());
        mEdtSearchWord.setOnKeyListener(searchKeyListener());
        mEdtSearchWord.setImeOptions(EditorInfo.IME_ACTION_DONE | EditorInfo.IME_FLAG_NO_FULLSCREEN
                | EditorInfo.IME_FLAG_NO_EXTRACT_UI);

        mBtnForward.setOnClickListener(btnForwardClickListener());
        mBtnBackward.setOnClickListener(btnBackwardClickListener());
        mBtnClose.setOnClickListener(btnCloseClickListener());

        Utils.setKeyboardListener(mTextEditor, new Utils.OnKeyboardVisibilityListener() {
            @Override
            public void onVisibilityChanged(boolean visible) {
                isKeyboardVisible = visible;
            }
        });
//        mScrollLayout.setScrollbarFadingEnabled(false);
//        mScrollLayout.setScrollingEnabled(false);
//        mTextEditor.setOnTouchListener(scrollLayoutTouchListener());
        gestureDetector = new GestureDetector(this, new GestureListener());

        //mTextEditor.setCustomSelectionActionModeCallback(textEditorActionMode());
        mTextEditor.setOnClickListener(textEditorClickListener());
        mTextEditor.setOnTouchListener(textEditorOnTouchListener());
        mTextEditor.addTextChangedListener(textEditorWatcher());

        newTouchSession();

        findViewById(R.id.btnScrollUp).setOnTouchListener(btnScrollUpListener());

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setVisibility(sharedPreferences.getBoolean(CheckableMenuItemsEnum.SHOW_TOOL_BAR.toString(), false) ? View.VISIBLE : View.GONE);
        getSupportActionBar().setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);
        findViewById(R.id.btnScrollDown).setOnTouchListener(btnScrollDownListener());
        mBtnFile.setOnClickListener(btnFileClickListener());
        mBtnView.setOnClickListener(btnViewClickListener(toolbar));

        registerForContextMenu(contextMenu);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.add(0, FIRST_ITEM_NUMBER, 0, FIRST_ITEM_VALUE);
        menu.add(0, SECOND_ITEM_NUMBER, 0, SECOND_ITEM_VALUE);
        menu.add(0, THIRD_ITEM_NUMBER, 0, THIRD_ITEM_VALUE);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    private void contextMenu() {
        openContextMenu(contextMenu);
    }

    private TextWatcher textEditorWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Layout tvLayout = mTextEditor.getLayout();
                String text = s.toString();

                if (automaticIndentation) {
                    if (start > 0 && text.length() > start && text.charAt(start) == '\n') {
                        automaticIndentationMethods(text, start);
                    }
                }

                totalLineCount = mTextEditor.getLineCount();

                if (tvLayout != null) {
                    currentLine = tvLayout.getLineForOffset(mTextEditor.getSelectionStart()) + 1;
                    totalColumnCount = tvLayout.getLineEnd(currentLine - 1) - tvLayout.getLineEnd(currentLine - 2);
                    currentColumn = mTextEditor.getSelectionStart() - tvLayout.getLineEnd(currentLine - 2);
                    selectionSize = 0;
                    endSelectionIndex = startSelectionIndex = tvLayout.getLineEnd(currentLine - 2) + currentColumn;
                }

                int byteLength = s.toString().getBytes().length;
                calculateFileSize(byteLength);

                configureStatusBar();
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mTextEditor.getLayout() != null)
                    mTextEditor.getMFastScroller().layoutHeightChangeListener(mTextEditor.getLayout().getHeight());

            }

            private void automaticIndentationMethods(String text, int start) {
                text = text.substring(0, start);
                int indexOfCurrentLetter = text.lastIndexOf("\n") + 1;

                mTextEditor.getText().insert(start + 1, getSpaceForAutomaticIndentation(text, indexOfCurrentLetter));
            }

            private String getSpaceForAutomaticIndentation(String text, int indexOfCurrentLetter) {
                String result = "";
                while (text.length() > indexOfCurrentLetter && text.charAt(indexOfCurrentLetter) == ' ') {
                    result = result + " ";
                    indexOfCurrentLetter++;
                }
                return result;
            }
        };
    }

    private void calculateFileSize(int byteLength) {
        if (byteLength < 100) {
            dimension = DIMENSION_BYTE;
            fileSize = (double) byteLength;
        } else if (byteLength < 102400) {
            dimension = DIMENSION_KILOBYTE;
            fileSize = (double) (byteLength / 1024) + (double) ((byteLength % 1024) / 100) / 10;
        } else {
            dimension = DIMENSION_MEGABYTE;
            fileSize = (double) (byteLength / 1048576) + (double) ((byteLength % 1048576) / 100000) / 10;
        }
    }

    @NonNull
    private View.OnClickListener btnViewClickListener(final Toolbar toolbar) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBtnView.setBackgroundColor(getResources().getColor(R.color.menu_selected));
                final ListPopupWindow popup = new ListPopupWindow(NotePadActivity.this);
                MenuAdapter adapter = new MenuAdapter(menuItemList, NotePadActivity.this, sharedPreferences);

                popup.setAdapter(adapter);
                popup.setAnchorView(mBtnView);
                popup.setWidth(VIEW_MENU_LENGTH);

                popup.setOnItemClickListener(onMenuItemClickListener(popup));
                popup.show();
                popup.getListView().setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);

                popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                    @Override
                    public void onDismiss() {
                        mBtnView.setBackgroundColor(getResources().getColor(R.color.standard_button_background));
                    }
                });
            }

            private AdapterView.OnItemClickListener onMenuItemClickListener(final ListPopupWindow popup) {
                return new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        boolean checkAction;
                        switch (position) {
                            case 0: //Show toolbar
                                checkAction = isCheckAction(view, position);
                                toolbar.setVisibility(checkAction ? View.VISIBLE : View.GONE);
                                break;
                            case 1: //Automatic indentation
                                checkAction = isCheckAction(view, position);
                                automaticIndentation = checkAction;
                                break;
                            case 2:
                                isCheckAction(view, position);
                                break;

                            case 3:
                                break;

                        }

                        popup.dismiss();
                    }

                    private boolean isCheckAction(View view, int position) {
                        SharedPreferences.Editor ed = sharedPreferences.edit();
                        CheckedTextView checkedTextView;
                        boolean checkAction;
                        checkedTextView = (CheckedTextView) view.findViewById(R.id.simple_my_menu_item);
                        checkAction = !checkedTextView.isChecked();
                        checkedTextView.setChecked(checkAction);
                        ed.putBoolean(menuItemList.get(position).getCheckableMenuItemsEnum().toString(), checkAction);
                        ed.apply();
                        return checkAction;
                    }
                };
            }

        };
    }

    @NonNull
    private View.OnClickListener btnFileClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBtnFile.setBackgroundColor(getResources().getColor(R.color.menu_selected));
                final ListPopupWindow popup = new ListPopupWindow(NotePadActivity.this);
                ArrayAdapter<String> adapter = new ArrayAdapter<>(NotePadActivity.this, android.R.layout.simple_list_item_1);
                popup.setAdapter(adapter);
                for (CharSequence sequence : getResources().getTextArray(R.array.menu_file)) {
                    adapter.add(sequence.toString());
                }
                popup.setAnchorView(mBtnFile);
                popup.setWidth(FILE_MENU_LENGTH);
                popup.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    private static final int NEW = 0;
                    private static final int OPEN = 1;

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        popup.dismiss();
                        switch (position) {
                            case NEW:
                                mTextEditor.setText("");
                                mTextEditor.getMFastScroller().resetThumbPos();
                                break;
                            case OPEN:
                                new FilePickerBuilder(NotePadActivity.this).withColor(android.R.color.holo_blue_bright)
                                        .withRequestCode(FilePicker.REQUEST_FILE)
                                        .withScopeType(FileScopeType.ALL)
                                        .isLoad(true)
                                        .launch();
                                break;
                            case 2:
                                saveAs();
                                break;
                        }
                    }
                });
                popup.show();
                popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                    @Override
                    public void onDismiss() {

                        mBtnFile.setBackgroundColor(getResources().getColor(R.color.standard_button_background));
                    }
                });
            }

        };
    }

    @NonNull
    private View.OnTouchListener btnScrollDownListener() {
        return new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
//                        mUIUpdater = new UIUpdater(new Runnable() {
//                            @Override
//                            public void run() {
//                                Layout layout = mTextEditor.getLayout();
//                                int startPosition = mScrollLayout.getYOffset() + mScrollLayout.getHeight();
//                                int line = layout.getLineForVertical(startPosition);
//                                if (line + 1 == layout.getLineCount()) {
//                                    mScrollLayout.fullScroll(View.FOCUS_DOWN);
//                                } else {
//                                    mScrollLayout.smoothScrollBy(0, -startPosition + layout.getLineBottom(line));
//                                }
//                            }
//                        });
//
//                        mUIUpdater.startUpdates();
//                        Log.d(TAG, "onTouch: button pressed");
                        mTextEditor.getMFastScroller().smoothBotScrollOneByObe();

                        break;
                    case MotionEvent.ACTION_UP:
                        Log.d(TAG, "onTouch: button up");
//                        mUIUpdater.stopUpdates();
                        mTextEditor.getMFastScroller().stopScrollMethods();
                        break;

                }

                return true;
            }
        };
    }

    @NonNull
    private View.OnTouchListener btnScrollUpListener() {
        return new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
//                        mUIUpdater = new UIUpdater(new Runnable() {
//                            @Override
//                            public void run() {
//                                Layout layout = mTextEditor.getLayout();
//                                int startPosition = mScrollLayout.getYOffset();
//                                int line = layout.getLineForVertical(startPosition);
//                                if (line == 0) {
//                                    mScrollLayout.fullScroll(View.FOCUS_UP);
//                                } else {
//                                    mScrollLayout.smoothScrollBy(0, -startPosition + layout.getLineTop(line - 1));
//                                }
//                            }
//                        });
//
//                        mUIUpdater.startUpdates();
//                        Log.d(TAG, "onTouch: button pressed");

                        mTextEditor.getMFastScroller().smoothTopScrollOneByObe();
                        break;
                    case MotionEvent.ACTION_UP:
                        mTextEditor.getMFastScroller().stopScrollMethods();
//                        Log.d(TAG, "onTouch: button up");
//                        mUIUpdater.stopUpdates();
                        break;
                }
                return true;

            }
        };
    }

    @NonNull //TODO: CHECK THIS METHOD
    private View.OnTouchListener scrollLayoutTouchListener() {
        return new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Layout layout = mTextEditor.getLayout();
                int line;
                float startPosition = event.getY();

                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE:
                        line = layout.getLineForVertical((int) startPosition);
                        event.setLocation(event.getX(), layout.getLineTop(line));
                    default:
                        Rect bounds = new Rect();
                        layout.getLineBounds(layout.getLineCount() - 1, bounds);
                        if (bounds.bottom < startPosition) {
                            event.setLocation(event.getX(), bounds.bottom);
                            mTextEditor.dispatchTouchEvent(event);
                            return true;
                        }
                }
                return false;
            }
        };
    }

    @NonNull
    private View.OnClickListener textEditorClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isKeyboardVisible) {
                    InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                    mTextEditor.getMFastScroller().resetThumbPos();
                }

            }
        };
    }

    @NonNull
    private ActionMode.Callback textEditorActionMode() {
        return new ActionMode.Callback() {

            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, android.view.MenuItem item) {
                return false;
            }
        };
    }

    @NonNull
    private View.OnClickListener btnBackwardClickListener() {
        return new View.OnClickListener() {
            public void onClick(View v) {
                String searchWord = mEdtSearchWord.getText().toString();
                mSearchForward = false;
                doSearch(searchWord);
            }
        };
    }

    @NonNull
    private View.OnClickListener btnForwardClickListener() {
        return new View.OnClickListener() {
            public void onClick(View v) {
                String searchWord = mEdtSearchWord.getText().toString();
                mSearchForward = true;
                doSearch(searchWord);
            }
        };
    }

    @NonNull
    private View.OnKeyListener searchKeyListener() {
        return new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((keyCode == KeyEvent.KEYCODE_ENTER || keyCode == KeyEvent.KEYCODE_DPAD_CENTER)
                        && event.getAction() == KeyEvent.ACTION_UP) {
                    if (mBtnForward.isEnabled()) {
                        mBtnForward.performClick();
                        return true;
                    }
                }
                return false;
            }
        };
    }

    @NonNull
    private View.OnClickListener btnCloseClickListener() {
        return new View.OnClickListener() {
            public void onClick(View v) {
                mSearchResult = null;
                mLlSearch.setVisibility(View.GONE);
            }
        };
    }

    @NonNull
    private TextWatcher searchTextWatcher() {
        return new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                boolean enabled = (s.length() > 0);
                mBtnForward.setEnabled(enabled);
                mBtnBackward.setEnabled(enabled);
                mSearchResult = null;
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        };
    }

    private View.OnTouchListener touchFilter(final LinearLayout mainView) {
        return new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (sharedPreferences.getBoolean(CheckableMenuItemsEnum.STYLUS_MODE.toString(), false)) {
                    for (int pointer = 0; pointer < event.getPointerCount(); pointer++) {
                        if ((event.getToolType(pointer) == MotionEvent.TOOL_TYPE_STYLUS)) {
                            MotionEvent stylusEvent = MotionEvent.obtain(
                                    event.getDownTime(),
                                    event.getEventTime(),
                                    event.getAction(),
                                    event.getX(pointer),
                                    event.getY(pointer),
                                    event.getMetaState());
                            mainView.dispatchTouchEvent(stylusEvent);
                            return true;
                        }
                    }
                } else {
                    mainView.dispatchTouchEvent(event);
                }
                return true;
            }
        };
    }

    private void initiateStatusBar() {
        tvLinesValue = (TextView) findViewById(R.id.tv_lines_value);
        tvColumnsValue = (TextView) findViewById(R.id.tv_column_value);
        tvSelectionValue = (TextView) findViewById(R.id.tv_selection_value);
        tvFileSizeValue = (TextView) findViewById(R.id.tv_file_size_value);

        tvColumnText = (TextView) findViewById(R.id.tv_column_text);
        tvFileSize = (TextView) findViewById(R.id.tv_file_size);
        tvSelection = (TextView) findViewById(R.id.tv_selection);
        tvColumnText = (TextView) findViewById(R.id.tv_column_text);

        currentLine = 0;
        totalLineCount = 0;

        currentColumn = 0;
        totalColumnCount = 0;

        startSelectionIndex = 0;
        endSelectionIndex = 0;
        selectionSize = 0;
        selectedLine = 0;
        selectedOffsetForLine = 0;

        fileSize = 0.0;
        dimension = DIMENSION_BYTE;

        configureStatusBar();
    }

    private void configureStatusBar() {
        setTextTvLineValue();
        setTextTvColumnValue();
        setTextTvSelection();
        setTextTvFileSize();
    }

    private void setTextTvFileSize() {
        String text = fileSize.toString();
        tvFileSizeValue.setText(text + " " + dimension);
    }

    private void setTextTvSelection() {
//        String text = startSelectionIndex + "/" + endSelectionIndex + "/" + selectionSize;
        String text = selectedOffsetForLine + "/" + selectedLine;
        if (selectionSize == 0) {
            tvSelectionValue.setVisibility(View.GONE);
            tvSelection.setVisibility(View.GONE);
            findViewById(R.id.vertical_view_3).setVisibility(View.GONE);
        } else {
            findViewById(R.id.vertical_view_3).setVisibility(View.VISIBLE);
            tvSelectionValue.setVisibility(View.VISIBLE);
            tvSelection.setVisibility(View.VISIBLE);
        }
        tvSelectionValue.setText(text);
    }

    private void setTextTvColumnValue() {
        String text = currentColumn + "/" + totalColumnCount;
        if (totalColumnCount == 0) {
            findViewById(R.id.vertical_view_2).setVisibility(View.GONE);
            tvColumnsValue.setVisibility(View.GONE);
            tvColumnText.setVisibility(View.GONE);

        } else {
            findViewById(R.id.vertical_view_2).setVisibility(View.VISIBLE);
            tvColumnsValue.setVisibility(View.VISIBLE);
            tvColumnText.setVisibility(View.VISIBLE);
        }
        tvColumnsValue.setText(text);
    }

    private void setTextTvLineValue() {
        String text = currentLine + "/" + totalLineCount;
        tvLinesValue.setText(text);
    }


    private void doSearch(String searchWord) {
        if (mSearchResult != null) {
            mProcOnSearchResult.search();
            return;
        }
        new SearchTask(this, searchWord, mTextEditor.getText(), false, false,
                mProcOnSearchResult);
    }

    abstract class PostSearchProcess implements OnSearchFinishedListener {
        abstract public void search();
    }

    private PostSearchProcess mProcOnSearchResult = new PostSearchProcess() {

        @Override
        public void search() {
            int cursor = mTextEditor.getSelectionStart();

            Record cursorRecord = new Record();
            cursorRecord.start = cursor;

            int cursorpos = Collections.binarySearch(mSearchResult, cursorRecord,
                    new Comparator<Record>() {
                        public int compare(Record object1, Record object2) {
                            return object1.start - object2.start;
                        }
                    });

            if (cursorpos >= 0) { // on the word

                if (mSearchForward) {
                    cursorpos++;
                    if (cursorpos < mSearchResult.size()) {
                        // found
                        highlight(cursorpos);
                    }
                } else {
                    cursorpos--;
                    if (cursorpos >= 0) {
                        // found
                        highlight(cursorpos);
                    }
                }

            } else { // not on the word
                cursorpos = -1 - cursorpos;
                if (mSearchForward) {
                    if (cursorpos < mSearchResult.size()) {
                        // found
                        highlight(cursorpos);
                    }
                } else {
                    cursorpos--;
                    if (cursorpos >= 0) {
                        // found
                        highlight(cursorpos);
                    }
                }
            }
        }

        @Override
        public void onSearchFinished(ArrayList<Record> data) {
            mSearchResult = data;
            search();
        }

        private void highlight(int pos) {
            Record r = mSearchResult.get(pos);
            mTextEditor.requestFocus();
            mTextEditor.setSelection(r.start, r.end);
        }

    };

    private View.OnTouchListener textEditorOnTouchListener() {
        return new View.OnTouchListener() {

            float x;
            float y;
            int currentDifBetweenStartAndCurrentLine;
            int stopSelection;

            int offsetX;

            @Override
            public boolean onTouch(View v, MotionEvent event) {

               /* if (sharedPreferences.getBoolean(CheckableMenuItemsEnum.SCROLL_MODE.toString(), false)){
                    return true;
                }*/
                Layout layout = ((EditText) v).getLayout();
                if (!gestureDetector.onTouchEvent(event)) {
                    if (doubleTouched)
                        switch (event.getAction()) {
                            case MotionEvent.ACTION_UP:
                                doubleTouched = false;
                                break;
                        }

                    if ((mTextEditor.getMFastScroller().isPointerInsideScrollBar(event.getX(), event.getY()) && mTextEditor.getMFastScroller().isVisible()) || mTextEditor.getMFastScroller().isDragging()) {
//
                        return false;
                    }
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            y = event.getY();
                            x = event.getX() + mTextEditor.getScrollX();
                            startXForSelection = x;
                            startLineForSelection = layout.getLineForVertical((int) y);

                            offsetX = mTextEditor.getOffset((int) x, (int) y);
                            startLetterIndex = offsetX;
                            mTextEditor.setSelection(offsetX);
                            v.callOnClick();
                            break;
                        case MotionEvent.ACTION_MOVE:
                            y = event.getY();
                            x = event.getX() + mTextEditor.getScrollX();
                            float currentDistance = Math.abs(startXForSelection - x);
                            int currentLine = layout.getLineForVertical((int) y);
                            currentDifBetweenStartAndCurrentLine = startLineForSelection - layout.getLineForVertical(mTextEditor.getScrollY());

                            offsetX = mTextEditor.getOffset((int) x, (int) y);
                            stopSelection = offsetX;

                            if ((currentDistance > FUZZY_DISTANCE) || currentDifBetweenStartAndCurrentLine != (startLineForSelection - currentLine)) {
                                currentDifBetweenStartAndCurrentLine = startLineForSelection - currentLine;
//                                if (x > layout.getLineMax(layout.getLineForVertical((int) y)))
                                if (startLetterIndex >= 0) {
                                    mTextEditor.setSelection(startLetterIndex, stopSelection);     // touch was at end of text
                                } else if (stopSelection > 0)
                                    mTextEditor.setSelection(startLetterIndex, stopSelection - 1);
                            }

                            break;
                        case MotionEvent.ACTION_UP:
                            newTouchSession();
                            break;
                        default:
                    }
                }
                calculationForStatusBarAfterSelection(layout);

                configureStatusBar();


                return true;

            }

            private void calculationForStatusBarAfterSelection(Layout layout) {
                int underCurrentLine = layout.getLineForOffset(offsetX);
                currentColumn = offsetX - layout.getLineEnd(underCurrentLine - 1);
                currentLine = underCurrentLine + 1;
                int startSelection = mTextEditor.getSelectionStart();
                int endSelection = mTextEditor.getSelectionEnd();
                if (endSelection < startSelection) {
                    int temp = endSelection;
                    endSelection = startSelection;
                    startSelection = temp;
                }
                startSelectionIndex = startSelection;
                endSelectionIndex = endSelection;
                selectionSize = endSelection - startSelection;
                selectedLine = Math.abs(currentDifBetweenStartAndCurrentLine) + 1;
                if (currentDifBetweenStartAndCurrentLine == 0) {
                    selectedOffsetForLine = selectionSize;
                } else if (startLetterIndex - stopSelection > 0) {
                    selectedOffsetForLine = mTextEditor.getOffset(3000, (int) y) - offsetX;
                } else {
                    selectedOffsetForLine = offsetX - mTextEditor.getOffset(0, (int) y);
                }
            }
        };
    }

    private void newTouchSession() {
        isTouchSessionUnbroken = true;
    }

    private void brokeTouchSession() {
        isTouchSessionUnbroken = false;
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public void onLongPress(MotionEvent e) {
            if (isTouchSessionUnbroken && !mTextEditor.getMFastScroller().isPointerInsideScrollBar(e.getX(), e.getY())) {
                contextMenu();
            }
            super.onLongPress(e);
        }

        // event when double tap occurs
        @Override
        public boolean onDoubleTap(MotionEvent e) {
            float x = e.getX();
            float y = e.getY();
            int offsetX = mTextEditor.getOffset((int) x, (int) y);
            String text = mTextEditor.getText().toString();

            int startSpaceCoordinate = text.substring(0, offsetX).lastIndexOf(" ", offsetX);
            if (startSpaceCoordinate < 0) {
                startSpaceCoordinate = 0;
            } else {
                startSpaceCoordinate++;
            }
            int startLineCoordinate = text.substring(0, offsetX).lastIndexOf("\n", offsetX);
            if (startLineCoordinate < 0) {
                startLineCoordinate = 0;
            } else {
                startLineCoordinate++;
            }
            int startCoordinate = startSpaceCoordinate > startLineCoordinate ? startSpaceCoordinate : startLineCoordinate;

            int spaceCoordinate = text.substring(offsetX).indexOf(" ");
            if (spaceCoordinate < 0) {
                spaceCoordinate = text.length();
            } else {
                spaceCoordinate += offsetX;
            }
            int endLineCoordinate = text.substring(offsetX).indexOf("\n");
            if (endLineCoordinate < 0) {
                endLineCoordinate = text.length();
            } else {
                endLineCoordinate += offsetX;
            }
            int endCoordinate = (spaceCoordinate < endLineCoordinate ? spaceCoordinate : endLineCoordinate);

            mTextEditor.setSelection(startCoordinate, endCoordinate);

            Log.d("Double Tap", "Tapped at: (" + x + "," + y + ")");

            doubleTouched = true;

            return true;
        }
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.item_search:
                mLlSearch.setVisibility(View.VISIBLE);
                return true;
            case R.id.item_copy:
                copyText();
                return true;
            case R.id.item_paste:
                deleteText();
                pasteText();
                return true;
            case R.id.cut:
                copyText();
                deleteText();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);

        }
    }

    private void pasteText() {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        if (clipboard.hasPrimaryClip()) {
            ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
            if (item != null) {
                mTextEditor.getText().insert(mTextEditor.getSelectionStart(), item.getText());
                calculateFileSize(mTextEditor.getText().toString().getBytes().length);
                setTextTvFileSize();
            }
        }

    }

    private void deleteText() {
        int startSelection = mTextEditor.getSelectionStart();
        int endSelection = mTextEditor.getSelectionEnd();
        if (endSelection < startSelection) {
            int temp = endSelection;
            endSelection = startSelection;
            startSelection = temp;
        }
        mTextEditor.getText().delete(startSelection, endSelection);
        calculateFileSize(mTextEditor.getText().toString().getBytes().length);
        setTextTvFileSize();
    }

    private void copyText() {
        int startSelection = mTextEditor.getSelectionStart();
        int endSelection = mTextEditor.getSelectionEnd();
        if (endSelection < startSelection) {
            int temp = endSelection;
            endSelection = startSelection;
            startSelection = temp;
        }
        String selectedText = mTextEditor.getText().toString().substring(startSelection, endSelection);
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        clipboard.setPrimaryClip(ClipData.newPlainText("simple text", selectedText));
    }

    @Override
    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {
        if (requestCode == FilePickerActivity.REQUEST_FILE
                && resultCode == RESULT_OK) {

            String filePath = data.
                    getStringExtra(FilePickerActivity.FILE_EXTRA_DATA_PATH);
            if (filePath != null) {
                KeywordHighlght.loadHighlight(this, filePath);
                if (data.getBooleanExtra(FilePickerActivity.IS_LOAD, true)) {

                    LoadTextTask mTask = new LoadTextTask(this, new LoadTextTask.OnFileLoadListener() {
                        @Override
                        public void onPreFileLoad() {

                        }

                        @Override
                        public void onFileLoaded(SpannableStringBuilder result, String filename, String charset, int linebreak, int mOffset) {
                            mTextEditor.setText(result);
                            mTextEditor.getMFastScroller().layoutHeightChangeListener(mTextEditor.getLayout().getHeight());
                            mTextEditor.getMFastScroller().refresh(2);
                        }
                    }, 0);
                    mTask.execute(filePath, "");
                } else {
                    save(filePath);

                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    private List<MenuItem> getMenuItemList() {
        List<MenuItem> result = new ArrayList<>();
        int index = 0;
        for (CharSequence sequence : getResources().getTextArray(R.array.checkable_menu_view)) {
            MenuItem newItem = new MenuItem(sequence.toString(), true);
            newItem.setCheckableMenuItemsEnum(CheckableMenuItemsEnum.values()[index]);
            result.add(newItem);
            index++;
        }

        for (CharSequence sequence : getResources().getTextArray(R.array.simple_menu_view)) {
            MenuItem newItem = new MenuItem(sequence.toString(), false);
            result.add(newItem);
        }

        return result;
    }

    private void save(String filename) {
        String charset = sharedPreferences.getString(CHAR_SET, "UTF-8");
        int linebreak = LineBreak.CRLF;
        save(charset, linebreak, filename);
    }

    private void save() {
        String filename = sharedPreferences.getString(FILE_NAME, "untitled");
        save(filename);
    }

    private void save(String charset, int linebreak, String filename) {
        if (filename != null) {
            CharSequence text = mTextEditor.getText();
            String lb = "\n";
            if (linebreak == LineBreak.CR) {
                lb = "\r";
            } else if (linebreak == LineBreak.CRLF) {
                lb = "\r\n";
            }

            new TextSaveTask(this, null, new Runnable() {
                public void run() {
                        /*saveHistory();
                        if (mProcAfterSaveConfirm != null) {
                            mProcAfterSaveConfirm.run();
                            mProcAfterSaveConfirm = null;
                        }
                        mEditor.setChanged(false);
                        onChanged();*/
                }
            }, false).execute(filename, charset, lb, text, "true");
        } else {
            saveAs();
        }
    }


    private void saveAs() {
        new FilePickerBuilder(NotePadActivity.this).withColor(android.R.color.holo_blue_bright)
                .withRequestCode(FilePicker.REQUEST_FILE)
                .withScopeType(FileScopeType.ALL)
                .isLoad(false)
                .launch();
    }


}

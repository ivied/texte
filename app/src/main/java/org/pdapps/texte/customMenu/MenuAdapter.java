package org.pdapps.texte.customMenu;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

import org.pdapps.texte.R;

import java.util.List;

public class MenuAdapter extends ArrayAdapter {

    private List<MenuItem> itemList;
    private Context context;
    private SharedPreferences sharedPreferences;

    public MenuAdapter(List<MenuItem> itemsList, Context context, SharedPreferences sPref) {
        super(context, R.layout.checkable_item, itemsList);
        this.itemList = itemsList;
        this.context = context;
        sharedPreferences = sPref;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        int type = getItemViewType(position);
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (type == 0) {
                v = inflater.inflate(R.layout.checkable_item, parent, false);
            } else {
                v = inflater.inflate(R.layout.simple_selectable_item, parent, false);
            }
        }

        final MenuItem menuItem = itemList.get(position);

        if (type == 0) {
            final CheckedTextView checkedTextView = (CheckedTextView) v.findViewById(R.id.simple_my_menu_item);
            checkedTextView.setChecked(sharedPreferences.getBoolean(menuItem.getCheckableMenuItemsEnum().toString(), false));
            checkedTextView.setText(menuItem.getItemName());
        } else {
            TextView simpleTextView = (TextView) v.findViewById(R.id.simple_my_menu_item);
            simpleTextView.setText(menuItem.getItemName());
        }

        return v;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return (itemList.get(position).getIsCheckable()) ? 0 : 1;
    }
}

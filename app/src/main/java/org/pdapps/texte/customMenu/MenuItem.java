package org.pdapps.texte.customMenu;

public class MenuItem {

    private String itemName;
    private Boolean isCheckable;
    private CheckableMenuItemsEnum checkableMenuItemsEnum;

    public MenuItem(String itemName, Boolean isCheckable) {
        this.itemName = itemName;
        this.isCheckable = isCheckable;
    }

    //<editor-fold desc="Getter Methods">
    public String getItemName() {
        return itemName;
    }

    public Boolean getIsCheckable() {
        return isCheckable;
    }

    public CheckableMenuItemsEnum getCheckableMenuItemsEnum() {
        return checkableMenuItemsEnum;
    }
    //</editor-fold>

    //<editor-fold desc="Setter Methods">
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public void setIsCheckable(Boolean isCheckable) {
        this.isCheckable = isCheckable;
    }


    public void setCheckableMenuItemsEnum(CheckableMenuItemsEnum checkableMenuItemsEnum) {
        this.checkableMenuItemsEnum = checkableMenuItemsEnum;
    }
    //</editor-fold>
}

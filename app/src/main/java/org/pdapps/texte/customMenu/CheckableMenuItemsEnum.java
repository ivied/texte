package org.pdapps.texte.customMenu;

public enum CheckableMenuItemsEnum {
    SHOW_TOOL_BAR("SHOW_TOOLBAR"),
    AUTOMATIC_INDENTATION("AUTOMATIC_INDENTATION"),
    STYLUS_MODE("STYLUS_MODE"),
    SCROLL_MODE("SCROLL_MODE");

    private final String text;

    private CheckableMenuItemsEnum(final String text){
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}

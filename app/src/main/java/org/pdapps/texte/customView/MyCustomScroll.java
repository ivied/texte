package org.pdapps.texte.customView;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.OverScroller;
import android.widget.Scroller;
import android.widget.SectionIndexer;

import org.pdapps.texte.R;
import org.pdapps.texte.UIUpdater;

import jp.sblo.pandora.jota.text.Layout;
import jp.sblo.pandora.jota.text.TextView;

public class MyCustomScroll {

    // Minimum number of pages to justify showing a fast scroll thumb
    private static int MIN_PAGES = 1;// Jota Text Editor
    // Scroll thumb not showing
    private static final int STATE_NONE = 0;
    // Not implemented yet - fade-in transition
    private static final int STATE_ENTER = 1;
    // Scroll thumb visible and moving along with the scrollbar
    private static final int STATE_VISIBLE = 2;
    // Scroll thumb being dragged by user
    private static final int STATE_DRAGGING = 3;
    // Scroll thumb fading out due to inactivity timeout
    private static final int STATE_EXIT = 4;

    private static final int TIME_FOR_PIXEL = 11;

    private static final String TAG = "MyCustomScroll";

    private Drawable mThumbDrawable;
    private Drawable mTrackDrawable;// Jota Text Editor

    private int mThumbH = 0;
    private int mThumbW;
    private int currentPositionThumbInView;
    private int minThumbH;
    private int topSpaceForThumb;
    private int fullScreenHeightTranslation;

    private int currentLayoutHeight;

    // Jota Text Editor
    private TextView mList;
    private boolean isScrollInitiate;
    private int mVisibleItem;
    private OverScroller overScroller;
    private UIUpdater mUIUpdater;
    //    private int mListOffset;// Jota Text Editor
    private boolean mLongList;

    private int mState;

    private SectionIndexer mSectionIndexer;

    private boolean mChangedBounds;

    private long mLastEventTime = 0;

    private float startMotionY = 0;
    private boolean isScrollUp;
    private float multiple;
    private static int FIRST_SCROLL_DELAY = 500;

    private static int NEXT_SCROLL_DELAY = 150;
    private static int SCROLL_DELAY_FOR_BUTTONS = 100;
    private static int finishYForSmoothScroll;
    private static boolean firstScroll;


    public MyCustomScroll(Context context, TextView listView) {
        mList = listView;
        init(context);
    }

    private void init(Context context) {
        // Get both the scrollbar states drawables
        final Resources res = context.getResources();
        useThumbDrawable(context, res.getDrawable(
                R.drawable.scrollbar_vertical_thumb));
        minThumbH = context.getResources().getDimensionPixelSize(R.dimen.minimum_thumb_height);
        topSpaceForThumb = context.getResources().getDimensionPixelOffset(R.dimen.scrollbar_thick);
        overScroller = new OverScroller(context);
        currentLayoutHeight = 0;
        isScrollInitiate = false;
        fullScreenHeightTranslation = 0;

//        int initialY = mList.getLayout().getOf
//        mList.scrollTo(0, );

        mState = STATE_NONE;
    }

    private void useThumbDrawable(Context context, Drawable drawable) {
        mThumbDrawable = drawable;
        mThumbW = context.getResources().getDimensionPixelSize(
                R.dimen.fastscroll_thumb_width);// Jota Text Editor
//        mThumbH = 0;// Jota Text Editor
        mChangedBounds = true;
    }

    public static int getFirstScrollDelay() {
        return FIRST_SCROLL_DELAY;
    }

    public static void setFirstScrollDelay(int firstScrollDelay) {
        FIRST_SCROLL_DELAY = firstScrollDelay;
    }

    public static int getNextScrollDelay() {
        return NEXT_SCROLL_DELAY;
    }

    public static void setNextScrollDelay(int nextScrollDelay) {
        NEXT_SCROLL_DELAY = nextScrollDelay;
    }

    public static int getScrollDelayForButtons() {
        return SCROLL_DELAY_FOR_BUTTONS;
    }

    public static void setScrollDelayForButtons(int scrollDelayForButtons) {
        SCROLL_DELAY_FOR_BUTTONS = scrollDelayForButtons;
    }

    public void setState(int state) {
        switch (state) {
            case STATE_NONE:
                mList.invalidate();
                break;
            case STATE_VISIBLE:
                if (mState != STATE_VISIBLE) { // Optimization
                    resetThumbPos();
                }
                break;
            case STATE_EXIT:
                int viewWidth = mList.getWidth();
                mList.invalidate(viewWidth - mThumbW, 0, viewWidth, mList.getHeight());
                break;
        }
        mState = state;
    }

    public void resetThumbPos() {
        final int viewWidth = mList.getWidth();
        // Bounds are always top right. Y coordinate get's translated during draw
        mList.getVisibleHeight();
        mThumbDrawable.setBounds(viewWidth - mThumbW, 0, viewWidth, mThumbH);
// Jota Text Editor
        mList.invalidate(viewWidth - mThumbW, 0, viewWidth, mList.getHeight());
    }

    public int getState() {
        return mState;
    }

    public boolean isDragging(){
        return getState() == STATE_DRAGGING;
    }

    public void stop() {
        setState(STATE_NONE);
    }

    public boolean isVisible() {
        return !(mState == STATE_NONE);
    }

    public void draw(Canvas canvas) {
        final int viewWidth;
        final int x;
        int y;
        if (mState == STATE_NONE || mState == STATE_EXIT) {
            // No need to draw anything
            return;
        }

        if (overScroller.computeScrollOffset()) {
            float offset = overScroller.getCurrY() - currentPositionThumbInView;
            scrollByOffset(offset);
            Log.d(TAG, "draw: " + offset);

        }

        y = currentPositionThumbInView + mList.getScrollY();

        viewWidth = mList.getWidth();
        x = mList.getScrollX();
//        mThumbH = (int) (mList.getHeight() * ((float) mList.getHeight() / (float) mList.getLayout().getHeight()));


        canvas.translate(x, y);
        mThumbDrawable.draw(canvas);
        canvas.translate(-x, -y);

        // If user is dragging the scroll bar, draw the alphabet overlay
// Jota Text Editor
        mList.invalidate(viewWidth - mThumbW, 0, viewWidth, mList.getHeight());
    }

    public void onSizeChanged(int w, int h) {
        recalculateThumbHeight(h);
        mList.scrollTo(0, mList.getScrollY() - mList.getHeight() + 15);
//        if (mThumbDrawable != null) {
//            resetThumbPos();
//        }
    }

    private void recalculateThumbHeight(int viewHeight) {
        int scrollTrack = viewHeight - topSpaceForThumb * 2;
        multiple = (float) currentLayoutHeight / viewHeight;
        if (multiple >= 1) {
            mThumbH = (int) (scrollTrack / multiple);
            mChangedBounds = true;
            if (mThumbH < minThumbH) {
                mThumbH = minThumbH;
            }
        }
    }

    public void onScroll(TextView view, int firstVisibleItem, int visibleItemCount,
                         int totalItemCount) {
// Jota Text Editor
//         Are there enough pages to require fast scroll? Recompute only if total count changes

        int viewHeight = mList.getHeight();

//        if (!isScrollInitiate) {
//            initialScrollMethods(firstVisibleItem, viewHeight, totalItemCount);
//        }

        int layoutHeight = totalItemCount + fullScreenHeightTranslation;

        int scrollTrack = viewHeight - topSpaceForThumb * 2;
        int cleanLayoutScroll = layoutHeight - viewHeight;
        int cleanScrollTrack = scrollTrack - mThumbH;
        if (overScroller.computeScrollOffset()) {
            if (overScroller.getCurrY() > currentPositionThumbInView && overScroller.getCurrY() < currentPositionThumbInView + mThumbH) {
                overScroller.forceFinished(true);
            }
        } else {

            multiple = (float) layoutHeight / (float) (viewHeight);

            if (visibleItemCount > 0) {
                mLongList = multiple >= MIN_PAGES;
            }

            if (!mLongList) {
                if (mState != STATE_NONE) {
                    setState(STATE_NONE);
                }
                return;
            }

            int currentPositionThumbInTrack = (int) (((long) cleanScrollTrack * firstVisibleItem) / cleanLayoutScroll);

            if (mState != STATE_DRAGGING) {
                currentPositionThumbInView = currentPositionThumbInTrack + topSpaceForThumb;
            }

            if (mChangedBounds) {
                resetThumbPos();
                mChangedBounds = false;
            }

            if (firstVisibleItem == mVisibleItem) {
                return;
            }

            mVisibleItem = firstVisibleItem;
            if (mState != STATE_DRAGGING) {
                if (mLongList) {
                    setState(STATE_VISIBLE);
                } else {
                    setState(STATE_NONE);
                }
            }
        }
    }


    private void initialScrollMethods(int firstItemIndex, int viewHeight, int totalHeight) {
        isScrollInitiate = true;
        fullScreenHeightTranslation = viewHeight + firstItemIndex - totalHeight;
        currentLayoutHeight = totalHeight + fullScreenHeightTranslation;
        recalculateThumbHeight(viewHeight);
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        boolean multiWindow = (float)mList.getLayout().getHeight()/mList.getHeight() > 1;
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (mState != STATE_NONE && multiWindow) {
                    if (isPointInsideThumb(ev.getX(), ev.getY())) {
                        startMotionY = ev.getY();
                        setState(STATE_DRAGGING);
                    } else {
                        setState(STATE_VISIBLE);
                        finishYForSmoothScroll = (int) ev.getY();
                        smoothScroll();
                    }
                }
                return true;
            default:
                return false;
        }
    }

    public boolean onTouchEvent(MotionEvent me) {
        if (mState == STATE_NONE) {
            return false;
        }

        final int viewHeight = mList.getHeight();
        final int action = me.getAction();
        float yOffset = (me.getY() - startMotionY);

        switch (action) {
            case MotionEvent.ACTION_UP:
//                overScroller.forceFinished(true);
                if (mState == STATE_DRAGGING || mState == STATE_VISIBLE) {

                    setState(STATE_VISIBLE);
                    stopUIUpdater();
                    Layout layout = mList.getLayout();
                    int position;
                    int currentLine;
                    int yPosition;
                    if (isScrollUp) {
                        position = mList.getScrollY() - 10;
                        currentLine = layout.getLineForVertical(position);
                        yPosition = layout.getLineTop(currentLine);
                    } else {
                        position = mList.getScrollY() + viewHeight + 10;
                        currentLine = layout.getLineForVertical(position);
                        yPosition = layout.getLineBottom(currentLine) - viewHeight + 15;
                    }
                    mList.scrollTo(0, yPosition);
                }
                return true;
            case MotionEvent.ACTION_MOVE:
                isScrollUp = yOffset <= 0;
                finishYForSmoothScroll = (int) me.getY();
                if (mState == STATE_DRAGGING) {
                    long now = System.currentTimeMillis();
                    long diff = (now - mLastEventTime);
                    if (diff > 30) {
                        mLastEventTime = now;

                        if (Math.abs(yOffset) >= 1) {
                            scrollByOffset(yOffset);
                            startMotionY = me.getY();

                            return false;
                        }
                    }
                    return true;
                }
                break;
            default:
                return false;
        }

        return false;
    }

    private void scrollByOffset(float offset) {
        int viewHeight = mList.getHeight();
        int scrollTrack = viewHeight - topSpaceForThumb * 2;
        float thumbInTrack = (currentPositionThumbInView - topSpaceForThumb) + offset;
        if (topOfScreen(thumbInTrack)) {
            thumbInTrack = 0;
        } else if (screenBottom(scrollTrack, thumbInTrack)) {
            thumbInTrack = scrollTrack - mThumbH;
        }
        int coefficientForConstantScroll = 0;

        if (mThumbH == minThumbH) {
            coefficientForConstantScroll = (int) (mThumbH * (thumbInTrack / (scrollTrack - mThumbH)));
        }

        int newY = (int) (((float) currentLayoutHeight / scrollTrack) * (thumbInTrack + coefficientForConstantScroll));

        if (currentLayoutHeight - newY < viewHeight) {
            newY = currentLayoutHeight - viewHeight;
        }
        if (currentLayoutHeight - newY < viewHeight) {
            newY = currentLayoutHeight - viewHeight;
        }
        currentPositionThumbInView = (int) thumbInTrack + topSpaceForThumb;
        mList.scrollTo(0, newY);
    }


    private boolean screenBottom(int scrollTrack, float newThumbY) {
        return newThumbY + mThumbH >= scrollTrack;
    }

    private boolean topOfScreen(float newThumbY) {
        return newThumbY <= 0;
    }

    public boolean isPointInsideThumb(float x, float y) {
        return x > mList.getWidth() - mThumbW * 1.5 && y >= currentPositionThumbInView && y <= currentPositionThumbInView + mThumbH;
    }

    public boolean isPointerInsideScrollBar(float x, float y) {
        return x > mList.getWidth() - mThumbW * 1.5 && y >= topSpaceForThumb && y <= mList.getHeight();
    }

    public void layoutHeightChangeListener(int height) {
        if (height != currentLayoutHeight) {
            recalculateLayoutHeight(height);
            recalculateThumbHeight(mList.getHeight());
        }

    }

    private void recalculateLayoutHeight(int height) {
        currentLayoutHeight = height + fullScreenHeightTranslation;
    }

    public void refresh(int position) {
        mList.scrollTo(0, position);
    }

    public void smoothBotScrollOneByObe() {
        boolean isMultiWindow = (float)mList.getLayout().getHeight()/mList.getHeight() > 1;
        if (isMultiWindow){
            mUIUpdater = new UIUpdater(new Runnable() {
                @Override
                public void run() {
                    setState(STATE_VISIBLE);
                    isScrollUp = false;
                    if (mList.getScrollY() < mList.getLayout().getHeight() - mList.getHeight())
                        mList.scrollTo(0, mList.getScrollY() + mList.getLineHeight());
                }
            });

            mUIUpdater.setUpdateInterval(SCROLL_DELAY_FOR_BUTTONS);
            mUIUpdater.startUpdates();
        }
    }

    public void smoothTopScrollOneByObe() {
        boolean isMultiWindow = (float)mList.getLayout().getHeight()/mList.getHeight() > 1;
        if (isMultiWindow) {
            mUIUpdater = new UIUpdater(new Runnable() {
                @Override
                public void run() {
                    setState(STATE_VISIBLE);
                    isScrollUp = true;
                    if (mList.getScrollY() > 0)
                        mList.scrollTo(0, mList.getScrollY() - mList.getLineHeight());
                }
            });
            mUIUpdater.setUpdateInterval(SCROLL_DELAY_FOR_BUTTONS);

            mUIUpdater.startUpdates();
        }
    }

    public void stopScrollMethods() {
        if (mUIUpdater != null) {
            mUIUpdater.stopUpdates();
            int position;
            int currentLine;
            int yPosition;
            int viewHeight = mList.getHeight();
            Layout layout = mList.getLayout();
            if (isScrollUp) {
                position = mList.getScrollY() - 10;
                currentLine = layout.getLineForVertical(position);
                yPosition = layout.getLineTop(currentLine);
            } else {
                position = mList.getScrollY() + viewHeight + 10;
                currentLine = layout.getLineForVertical(position);
                yPosition = layout.getLineBottom(currentLine) - viewHeight + 15;
            }
            mList.scrollTo(0, yPosition);
        }
    }

    //scroll after press on track

    private void smoothScroll() {
        firstScroll = true;
        mUIUpdater = new UIUpdater(new Runnable() {
            @Override
            public void run() {
                setState(STATE_VISIBLE);
                isScrollUp = currentPositionThumbInView > finishYForSmoothScroll;
                if (thumbComeToNeededPosition(finishYForSmoothScroll)) {
                    stopUIUpdater();
                    return;
                }

                if (firstScroll) {
                    firstScroll = false;
                } else {
                    mUIUpdater.setUpdateInterval(NEXT_SCROLL_DELAY);
                }

                mList.scrollTo(0, calculateNewYCoordinate());
            }

        });
        mUIUpdater.setUpdateInterval(FIRST_SCROLL_DELAY);
        mUIUpdater.startUpdates();
    }

    private int calculateNewYCoordinate() {
        int newYScrollCoordinate;
        if (isScrollUp) {
            newYScrollCoordinate = mList.getScrollY() - mList.getHeight();
            if (newYScrollCoordinate < 0) {
                newYScrollCoordinate = 0;
            }
        } else {
            newYScrollCoordinate = mList.getScrollY() + mList.getHeight();
            if (newYScrollCoordinate > mList.getLayout().getHeight() - mList.getHeight()) {
                newYScrollCoordinate = mList.getLayout().getHeight() - mList.getHeight();
            }
        }
        return newYScrollCoordinate;
    }

    private boolean thumbComeToNeededPosition(int finishY) {
        return finishY > currentPositionThumbInView && finishY < currentPositionThumbInView + mThumbH;
    }

    private void stopUIUpdater() {
        if (mUIUpdater != null)
        mUIUpdater.stopUpdates();
    }
}

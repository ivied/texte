package org.pdapps.texte.customView;

import android.content.Context;
import android.support.v7.widget.ListPopupWindow;
import android.util.AttributeSet;

import org.pdapps.texte.R;

public class ClearAnchorListWindow extends ListPopupWindow{
    Context mContext;

    public ClearAnchorListWindow(Context context) {
        super(context);
        mContext = context;
    }

    public ClearAnchorListWindow(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public ClearAnchorListWindow(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    public ClearAnchorListWindow(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mContext = context;
    }


    @Override public void dismiss() {
        getAnchorView().setBackgroundColor(mContext.getResources().getColor(R.color.standard_button_background));
        super.dismiss();
    }

    @Override public Object getSelectedItem() {
        return super.getSelectedItem();
    }
}

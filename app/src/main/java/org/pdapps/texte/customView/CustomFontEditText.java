package org.pdapps.texte.customView;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import jp.sblo.pandora.jota.text.EditText;

public class CustomFontEditText extends EditText {

    public CustomFontEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomFontEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomFontEditText(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "DroidSansMono.ttf");
            setTypeface(tf);
        }
    }

}
